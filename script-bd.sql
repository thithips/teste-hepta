-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema funcionarios-bd
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema funcionarios-bd
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `funcionarios-bd` DEFAULT CHARACTER SET utf8 ;
USE `funcionarios-bd` ;

-- -----------------------------------------------------
-- Table `funcionarios-bd`.`Setor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `funcionarios-bd`.`Setor` (
  `ID_SETOR` INT NOT NULL AUTO_INCREMENT,
  `NOME` VARCHAR(45) NULL,
  PRIMARY KEY (`ID_SETOR`))
ENGINE = InnoDB
COMMENT = '	';


-- -----------------------------------------------------
-- Table `funcionarios-bd`.`Funcionario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `funcionarios-bd`.`Funcionario` (
  `ID_FUNCIONARIO` INT NOT NULL AUTO_INCREMENT,
  `NOME` VARCHAR(80) NULL,
  `NU_SALARIO` DOUBLE NULL,
  `DS_EMAIL` VARCHAR(80) NULL,
  `NU_IDADE` INT NULL,
  `FK_SETOR` INT NOT NULL,
  PRIMARY KEY (`ID_FUNCIONARIO`),
  INDEX `fk_Funcionario_Setor_idx` (`FK_SETOR` ASC) VISIBLE,
  CONSTRAINT `fk_Funcionario_Setor`
    FOREIGN KEY (`FK_SETOR`)
    REFERENCES `funcionarios-bd`.`Setor` (`ID_SETOR`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
